package com.mrsgameandsmartwatch.trainingtroop

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.mrsgameandsmartwatch.trainingtroop.model.*
import com.mrsgameandsmartwatch.trainingtroop.repository.*
import java.util.*


class HiHealthViewModel(application: Application) : AndroidViewModel(application) {
    private val weightDataRepository: WeightDataRepository = WeightDataRepository(getApplication())
    private val sleepDataRepository: SleepDataRepository = SleepDataRepository(getApplication())
    private val userDataRepository: UserDataRepository = UserDataRepository(getApplication())
    private val singleActivitiesRepository: SingleActivitiesRepository =
        SingleActivitiesRepository(getApplication())
    private val authorizationRepository: AuthorizationRepository =
        AuthorizationRepository(getApplication())
    private val stepsRepository: StepsRepository = StepsRepository(getApplication())
    private val caloriesRepository: CaloriesRepository = CaloriesRepository(getApplication())
    private val heartRateRepository: HeartRateRepository =
        HeartRateRepository(getApplication(), Gson())
    private val rriRepository: RriRepository = RriRepository(getApplication(), Gson())
    private val realTimeSportsRepository: RealTimeSportsRepository =
        RealTimeSportsRepository(getApplication())
    private val heartRate: MutableLiveData<HeartBeatRate> = MutableLiveData<HeartBeatRate>()
    private val gender: MutableLiveData<HiHealthGender> = MutableLiveData<HiHealthGender>()
    private val birthday = MutableLiveData<Date>()
    private val height = MutableLiveData<String>()
    private val weight: MutableLiveData<WeightData> = MutableLiveData<WeightData>()
    private val rri: MutableLiveData<RRIResponseData> = MutableLiveData<RRIResponseData>()
    private val stepSum: MutableLiveData<List<DailyStepsData>> =
        MutableLiveData<List<DailyStepsData>>()
    private val stepsChallengeDays = MutableLiveData<Int>()
    private val caloriesSum: MutableLiveData<List<DailyCaloriesData>> =
        MutableLiveData<List<DailyCaloriesData>>()
    private val realTimeSportData: MutableLiveData<RealTimeSportData> =
        MutableLiveData<RealTimeSportData>()
    private val walkActivityData: MutableLiveData<List<WalkData>> =
        MutableLiveData<List<WalkData>>()
    private val runActivityData: MutableLiveData<List<RunData>> =
        MutableLiveData<List<RunData>>()
    private val cyclingActivityData: MutableLiveData<List<CyclingData>> =
        MutableLiveData<List<CyclingData>>()
    private val sleepData: MutableLiveData<List<SleepData>> =
        MutableLiveData<List<SleepData>>()
    private val authStatus: MutableLiveData<PermissionStatus> = MutableLiveData<PermissionStatus>()
    fun fetchHealthInfo() {
        userDataRepository.getGender(gender::postValue)
        userDataRepository.getBirthday(birthday::postValue)
        userDataRepository.getHeight(height::postValue)
        weightDataRepository.getLatestWeightData(weight::postValue)
        //heartRateRepository.track(heartRate::postValue)
        //realTimeSportsRepository.track(realTimeSportData::postValue)
    }

    fun requestAuthorization() {
        authorizationRepository.requestAllPermissions()
    }

    fun checkAuthorizationStatus() {
        authorizationRepository.getAuthorizationStatus(authStatus::postValue)
    }

    fun saveWeight(weight: Double) {
        val data: WeightData = WeightData.Builder()
            .setWeight(weight)
            .build()
        weightDataRepository.saveWeightData(
            data,
            { errorCode -> if (errorCode === 0) fetchHealthInfo() })
    }

    fun stopTrackingDynamicData() {
        heartRateRepository.stopTracking()
        realTimeSportsRepository.stopTracking()
    }

    fun trackRri() {
        rriRepository.track(rri::postValue)
    }

    fun stopTrackingRri() {
        rriRepository.stopTracking()
    }

    fun fetchStepsChallengeDays(since: Date, till: Date) {
        stepsRepository.getStepsChallengeDaysCompleted(since, till, stepsChallengeDays::postValue)
    }

    fun fetchStepSum(since: Date, till: Date) {
        stepsRepository.getStepsData(since, till, stepSum::postValue)
    }

    fun fetchCaloriesSum(since: Date, till: Date) {
        caloriesRepository.getCaloriesData(since, till, caloriesSum::postValue)
    }

    fun fetchSleepData(since: Date, till: Date) {
        sleepDataRepository.getSleepData(since, till, sleepData::postValue)
    }

    fun fetchWalkData(since: Date, till: Date) {
        singleActivitiesRepository.getWalkData(since, till, walkActivityData::postValue)
    }

    fun fetchRunData(since: Date, till: Date) {
        singleActivitiesRepository.getRunData(since, till, runActivityData::postValue)
    }

    fun fetchCyclingData(since: Date, till: Date) {
        singleActivitiesRepository.getCyclingData(since, till, cyclingActivityData::postValue)
    }

    fun deleteWeightData(since: Date, till: Date) {
        weightDataRepository.deleteWeightData(
            since,
            till,
            { errorCode -> if (errorCode == 0) fetchHealthInfo() })
    }

    //region LiveData getters
    fun getHeartRate(): LiveData<HeartBeatRate> {
        return heartRate
    }

    fun getGender(): LiveData<HiHealthGender> {
        return gender
    }

    fun getBirthday(): LiveData<Date> {
        return birthday
    }

    fun getHeight(): LiveData<String> {
        return height
    }

    fun getWeight(): LiveData<WeightData> {
        return weight
    }

    fun getRri(): LiveData<RRIResponseData> {
        return rri
    }

    fun getStepSum(): LiveData<List<DailyStepsData>> {
        return stepSum
    }

    fun getStepsChallengeDays(): LiveData<Int> {
        return stepsChallengeDays
    }

    fun getCaloriesSum(): LiveData<List<DailyCaloriesData>> {
        return caloriesSum
    }

    fun getRealTimeSportData(): LiveData<RealTimeSportData> {
        return realTimeSportData
    }

    fun getSleepData(): LiveData<List<SleepData>> {
        return sleepData
    }

    fun getWalkActivityData(): LiveData<List<WalkData>> {
        return walkActivityData
    }

    fun getRunActivityData(): LiveData<List<RunData>> {
        return runActivityData
    }

    fun getCyclingActivityData(): LiveData<List<CyclingData>> {
        return cyclingActivityData
    }

    fun getAuthStatus(): LiveData<PermissionStatus> {
        return authStatus
    } //endregion
}