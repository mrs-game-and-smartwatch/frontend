package com.mrsgameandsmartwatch.trainingtroop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.activity_troop.*
import org.json.JSONArray
import org.json.JSONObject


class TroopActivity : AppCompatActivity() {
    private lateinit var members: ArrayList<Pair<Int, String>>

    private var troopId = -1

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: TroopAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_troop)
        // Set up toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        troopId = intent.getIntExtra(TROOP_ID, -2)
        members = ArrayList()
        Log.d("TROOPID", "troopId: $troopId")

        initMemberEntries()
    }

    // Handle pressing <- on toolbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun getGroupData() {
        val networking = Networking.getInstance(this)
        val url = "${networking.server}/api/troops/$troopId/"

        val jsonRequest =
            JsonObjectRequest(
                Request.Method.GET, url, null,
                Response.Listener<JSONObject> { response ->
                    Log.d("MEMBERGET", "Volley: All good")

                    findViewById<TextView>(R.id.title_troop_name).text = response.getString("name")
                    findViewById<TextView>(R.id.number_troop_score).text =
                        response.getInt("score").toString()

                    val profiles = response.getJSONArray("profiles")
                    for (i in 0 until profiles.length()) {
                        val elem = profiles.getJSONObject(i)
                        members.add(elem.getInt("id") to elem.getString("username"))
                        Log.d("MEMBERGET", elem.getString("username"))
                    }

                    viewAdapter.addTroopsToList(members)
                },
                Response.ErrorListener { error ->
                    Log.e("MEMBERGET", "Volley Error: $error")
                    // Add default troops
                    viewAdapter.addTroopsToList(arrayListOf(1 to "You are alone", 2 to "Because you are offline :("))
                })
        networking.addToRequestQueue(jsonRequest)
    }

    private fun initMemberEntries() {
        // The manager for the layout where the entries are put into
        viewManager = LinearLayoutManager(this)
        // Initialize adapter with empty member list
        viewAdapter = TroopAdapter(arrayListOf())

        // Populate recyclerView with empty content
        recyclerView = member_list.apply {
            // use linear layout manager
            layoutManager = viewManager
            adapter = viewAdapter
        }

        getGroupData()
    }
}