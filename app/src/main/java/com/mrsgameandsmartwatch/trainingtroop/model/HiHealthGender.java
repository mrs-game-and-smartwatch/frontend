package com.mrsgameandsmartwatch.trainingtroop.model;

public enum HiHealthGender {
    UNKNOWN,
    MALE,
    FEMALE
}
