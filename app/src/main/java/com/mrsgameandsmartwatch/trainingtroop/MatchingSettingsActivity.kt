package com.mrsgameandsmartwatch.trainingtroop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_matching_settings.*

class MatchingSettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matching_settings)
        // Set up toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        emulateRadioButtons()
    }

    // Handle pressing <- on toolbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    // Reimplement radio button exclusivity because android is retarded
    private fun emulateRadioButtons() {
        age_choice_any.setOnClickListener {
            // Make age range inputs uneditable
            min_age.isEnabled = false
            max_age.isEnabled = false

            age_choice_range.isChecked = false
            age_choice_any.isChecked = true
        }
        age_choice_range.setOnClickListener {
            // Make age range inputs editable again
            min_age.isEnabled = true
            max_age.isEnabled = true

            age_choice_any.isChecked = false
            age_choice_range.isChecked = true
        }
    }

    fun saveSettings(v: View) {
        // TODO: this is the onClick() function for the save settings button
    }
}