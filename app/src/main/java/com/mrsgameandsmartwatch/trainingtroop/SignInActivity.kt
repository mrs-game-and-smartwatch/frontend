package com.mrsgameandsmartwatch.trainingtroop

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

const val USERNAME = "com.mrsgameandsmartwatch.trooptrainer.USERNAME"
const val USERSCORE = "com.mrsgameandsmartwatch.trooptrainer.USERSCORE"
const val USERVERIFIED = "com.mrsgameandsmartwatch.trooptrainer.USERVERIFIED"

class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        // Set up toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun signIn(v: View) {
        val username = findViewById<EditText>(R.id.username).text

        val networking = Networking.getInstance(this)
        val url = "${networking.server}/api/profiles/login/?username=$username"
        val jsonRequest =
            JsonObjectRequest(
                Request.Method.GET, url, null,
                Response.Listener { response ->
                    Log.d("LOGIN", "Volley: All good")

                    val context = this
                    GlobalScope.launch { User.createInstance(context, response) }

                    // Go to profile activity
                    val intent = Intent(this, ProfileActivity::class.java)
                    intent.putExtra(USERNAME, response.getString("username"))
                    intent.putExtra(USERSCORE, response.getInt("score"))
                    intent.putExtra(USERVERIFIED, response.getBoolean("verified"))
                    startActivity(intent)
                },

                Response.ErrorListener { error ->
                    Log.e("LOGIN", "Volley Error: $error")
                    Toast.makeText(
                        applicationContext,
                        "Login failed, fallback to default login...",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    // TODO: Remove this later for "production"
                    startActivity(Intent(this, ProfileActivity::class.java))
                })
        networking.addToRequestQueue(jsonRequest)
    }
}