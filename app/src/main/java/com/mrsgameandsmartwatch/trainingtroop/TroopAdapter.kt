package com.mrsgameandsmartwatch.trainingtroop

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

const val TROOP_ID = "com.mrsgameandsmartwatch.trainingtroop.TROOP_ID"
const val USER_ID = "com.mrsgameandsmartwatch.trainingtroop.USER_ID"

enum class EntryType {
    TROOP,
    MEMBER
}

class TroopAdapter(private val _troopList: ArrayList<Pair<Int, String>>, private val entryType: EntryType = EntryType.TROOP)
    : RecyclerView.Adapter<TroopAdapter.TroopViewHolder>() {

    // Property with getter for troopList
    val troopList: ArrayList<Pair<Int, String>>
        get() = _troopList

    fun addTroopsToList(newItems: ArrayList<Pair<Int, String>>) {
        val oldSize = itemCount
        // avoid duplicates
        for (i in newItems) {
            if (i !in _troopList) {
                _troopList.add(i)
            }
        }
        this.notifyItemRangeInserted(oldSize, itemCount)
    }

    /*
        Provide a reference to the views for each data item
        Complex data items may need more than one view per item, and
        you provide access to all the views for a data item in a view holder.
        Each data item is just a string in this case that is shown in a TextView.
     */
    inner class TroopViewHolder(val view: FrameLayout) : RecyclerView.ViewHolder(view)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : TroopAdapter.TroopViewHolder {

        // Create a new view from the layout
        val troopEntry = LayoutInflater.from(parent.context)
            .inflate(R.layout.troop_entry, parent, false) as FrameLayout


        return TroopViewHolder(troopEntry)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: TroopViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.troop_name).text =
            troopList[position].second

        // If desired, modify your view here (i.e. set button onClicks, change color, margins, etc.)
        when (entryType) {
            EntryType.TROOP -> {
                fun goToTroopView(v: View) {
                    val intent = Intent(v.context, TroopActivity::class.java).apply {
                        putExtra(TROOP_ID, troopList[position].first)
                    }
                    v.context.startActivity(intent)
                }

                holder.view.setOnClickListener(::goToTroopView)
                holder.view.findViewById<ImageButton>(R.id.goto_btn)
                    .setOnClickListener(::goToTroopView)
            }
            EntryType.MEMBER -> {
                holder.view.findViewById<ImageButton>(R.id.goto_btn).setOnClickListener {
                    // TODO: Go to that user's profile view
                    //  - fetch the USER_ID in onCreate of the new view
                    fun goToProfileView(v: View) {
                        val intent = Intent(v.context, ProfileActivity::class.java).apply {
                            putExtra(USER_ID, troopList[position].first)
                        }
                        v.context.startActivity(intent)
                    }

                    holder.view.setOnClickListener(::goToProfileView)
                    holder.view.findViewById<ImageButton>(R.id.goto_btn)
                        .setOnClickListener(::goToProfileView)
                }
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int = troopList.size
}
