package com.mrsgameandsmartwatch.trainingtroop

import android.content.Intent
import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.profile_bottom_sheet.*
import kotlinx.android.synthetic.main.profile_main.*
import kotlinx.android.synthetic.main.userinfo_basic.*
import kotlinx.coroutines.awaitAll
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text

class ProfileActivity : AppCompatActivity() {
    private lateinit var troops: ArrayList<Pair<Int, String>>

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: TroopAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        // Set up toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        troops = ArrayList()

        findViewById<TextView>(R.id.text_username).text = intent.getStringExtra(USERNAME)
        findViewById<TextView>(R.id.text_score).text = intent.getIntExtra(USERSCORE, -1).toString()

        if (intent.getBooleanExtra(USERVERIFIED, true)) {
            val image = findViewById<ImageView>(R.id.unverified_warning)
            image.setImageResource(R.drawable.ic_check_circle_black_24dp)
            image.imageTintList = ColorStateList.valueOf(
                resources.getColor(R.color.matchGreen, null)
            )
        }

        initTroopEntries()

        val bottomSheet: LinearLayout = findViewById(R.id.bottom_sheet)
        val sheetBehavior = BottomSheetBehavior.from(bottomSheet)

        setupSheetBehavior(sheetBehavior)
    }

    // Handle pressing <- on toolbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setupSheetBehavior(sheetBehavior: BottomSheetBehavior<LinearLayout>) {
        // Toggle expand button arrow direction on button click
        btn_bottom_sheet.setOnClickListener {
            if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                btn_bottom_sheet.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
            } else {
                sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                btn_bottom_sheet.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
            }
        }

        // Toggle expand button arrow direction on sheet state change (e.g. by swiping)
        sheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull view:View, newState:Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {}
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        btn_bottom_sheet.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        btn_bottom_sheet.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {}
                    BottomSheetBehavior.STATE_SETTLING -> {}
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {}
                }
            }
            override fun onSlide(@NonNull view:View, v:Float) {}
        })
    }

    private fun getTroopList() {
        val userId = User.getInstance().id
        val networking = Networking.getInstance(this)
        val url = "${networking.server}/api/profiles/$userId/troops/"

        val jsonRequest =
            JsonArrayRequest(
                Request.Method.GET, url, null,
                Response.Listener { response ->
                    Log.d("TROOPGET", "Volley: All good")

                    for (i in 0 until response.length()) {
                        val elem = response.getJSONObject(i)
                        troops.add(Pair(elem.getInt("id"), elem.getString("name")))
                        Log.d("TROOPGET", elem.getString("name"))
                    }

                    viewAdapter.addTroopsToList(troops)
                    no_items_msg.visibility = View.GONE
                },
                Response.ErrorListener { error ->
                    Log.e("TROOPGET", "Volley Error: $error")
                    // Add default troops
                    viewAdapter.addTroopsToList(arrayListOf(1 to "I'm your offline Troop :("))
                    no_items_msg.visibility = View.GONE
                })
        networking.addToRequestQueue(jsonRequest)
    }

    override fun onResume() {
        super.onResume()
        getTroopList()
    }

    private fun initTroopEntries() {
        // The manager for the layout where the entries are put into
        viewManager = LinearLayoutManager(this)
        // Initialize empty adapter
        viewAdapter = TroopAdapter(arrayListOf())

        // Populate recyclerView with empty content
        recyclerView = findViewById<RecyclerView>(R.id.troop_list).apply {
            // use linear layout manager
            layoutManager = viewManager
            adapter = viewAdapter
        }

        getTroopList()
    }

    fun goToMatching(v: View) {
        val intent = Intent(this, MatchingActivity::class.java)
        startActivity(intent)
    }
}
