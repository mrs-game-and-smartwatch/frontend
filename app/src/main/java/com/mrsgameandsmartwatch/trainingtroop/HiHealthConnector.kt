package com.mrsgameandsmartwatch.trainingtroop

import android.content.DialogInterface
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.mrsgameandsmartwatch.trainingtroop.model.*
import com.mrsgameandsmartwatch.trainingtroop.util.Utils
import java.lang.Math.sqrt
import java.util.*

class HiHealthConnector(
    val viewModel: HiHealthViewModel,
    val owner: AppCompatActivity,
    val callback: (Boolean, Int) -> Unit
) {

    public var gender: HiHealthGender? = null
    public var height: String? = null
    public var birthday: Date? = null
    public var weight: WeightData? = null
    public var stepSum: List<DailyStepsData>? = null
    public var caloriesSum: List<DailyCaloriesData>? = null
    public var cyclingData: List<CyclingData>? = null
    public var walkData: List<WalkData>? = null
    public var runData: List<RunData>? = null

    public var score: Int = 0

    fun setup() {
        check()
        setupAuthObservers()
        setupUserDataObservers()
        fetchData()
    }


    fun calcScore(): Int {


        val stepFactor: Double = 0.1
        val calFactor: Double = 0.01
        val cyclingFactor: Double = 0.0000001
        val walkingFactor: Double = 0.000001
        val runningFactor: Double = 0.00001


        var stepScore: Double = 0.0
        var weight: Double = 0.1
        for (i in stepSum ?: ArrayList<DailyStepsData>()) {
            //Log.e("Score", i.steps.toString())
            //stepScore = stepScore + weight * (i.steps - stepScore)
            stepScore += i.steps
        }
        var totalScore: Double = stepFactor * stepScore
        Log.v("Score1", totalScore.toString())

        var calScore: Double = 0.0
        weight = 0.1

        for (i in caloriesSum ?: ArrayList<DailyCaloriesData>()) {
            //calScore = calScore + weight * (i.calories - calScore)
            calScore += i.calories
        }

        totalScore += calFactor * calScore
        Log.v("Score2", totalScore.toString())

        var runScore: Double = 0.0
        weight = 0.1
        for (i in runData ?: ArrayList<RunData>()) {
            //runScore = runScore + weight * (i.totalDistance*i.totalTime - runScore)
            runScore += i.totalDistance * i.totalTime
        }
        totalScore += runningFactor * runScore
        Log.v("Score3", totalScore.toString())

        var walkScore: Double = 0.0
        weight = 0.1
        for (i in walkData ?: ArrayList<WalkData>()) {
            //Log.e("important", (walkScore + weight * (i.totalDistance*i.totalTime - walkScore)).toString())
            //walkScore = walkScore + weight * (i.totalDistance*i.totalTime - walkScore)
            walkScore += i.totalDistance * i.totalTime
        }

        totalScore += walkingFactor * walkScore
        Log.v("Score4", totalScore.toString())

        var cyclingScore: Double = 0.0
        weight = 0.1
        for (i in cyclingData ?: ArrayList<CyclingData>()) {
            //cyclingScore = cyclingScore + weight * (i.totalDistance*i.totalTime   - cyclingScore)
            cyclingScore += i.totalDistance * i.totalTime
        }

        totalScore += cyclingFactor * cyclingScore

        var re: Int
        Log.v("Score5", totalScore.toString())

        if (totalScore > 0.01) {
            re = sqrt(totalScore).toInt()
        } else {
            re = totalScore.toInt()
        }
        Log.v("final_score", re.toString())
        if (stepSum != null && caloriesSum != null && runData != null && walkData != null && cyclingData != null) {
            callback(true, re)
        }
        return re

    }

    private fun setupAuthObservers() {
        viewModel.getAuthStatus().observe(owner, Observer { permissionStatus ->
            // Update the UI, in this case, a TextView.
            when (permissionStatus) {
                PermissionStatus.REQUEST_AUTHORIZATION,
                PermissionStatus.PERMISSION_NOT_GRANTED -> askForPermissions()
                PermissionStatus.GOT_PERMISSION -> viewModel.fetchHealthInfo()
                else -> callback(false, 0)
            }
        })
    }

    private fun fetchData() {
        viewModel.fetchWalkData(getQuerySinceDate(14), getQueryTillDate(0))
        viewModel.fetchCyclingData(getQuerySinceDate(14), getQueryTillDate(0))
        viewModel.fetchRunData(getQuerySinceDate(14), getQueryTillDate(0))
        //viewModel.fetchSleepData(getQuerySinceDate(14), getQueryTillDate(0))
        viewModel.fetchStepSum(getQuerySinceDate(14), getQueryTillDate(0))
        viewModel.fetchCaloriesSum(getQuerySinceDate(14), getQueryTillDate(0))
    }

    private fun setupUserDataObservers() {
        viewModel.getGender().observe(owner, Observer { value -> gender = value })
        viewModel.getBirthday().observe(owner, Observer { value -> birthday = value })
        viewModel.getHeight().observe(owner, Observer { value -> height = value })
        viewModel.getWeight().observe(owner, Observer { value -> weight = value })
        viewModel.getWalkActivityData()
            .observe(owner, Observer { value -> walkData = value; calcScore() })
        viewModel.getRunActivityData()
            .observe(owner, Observer { value -> runData = value; calcScore() })
        viewModel.getCyclingActivityData()
            .observe(owner, Observer { value -> cyclingData = value; calcScore() })
//        viewModel.getSleepData()
//            .observe(owner, Observer{ value ->  sleep = value })
        viewModel.getStepSum()
            .observe(owner, Observer { value -> stepSum = value; calcScore() })
        viewModel.getCaloriesSum()
            .observe(owner, Observer { value -> caloriesSum = value; calcScore() })
    }

    fun askForPermissions() {
        AlertDialog.Builder(owner)
            .setTitle(R.string.permissions_dialog_title)
            .setMessage(R.string.permissions_dialog_message)
            .setPositiveButton(
                R.string.settings
            ) { dialog, which -> viewModel.requestAuthorization() }
            .setNegativeButton(
                android.R.string.no
            ) { dialog: DialogInterface?, which: Int -> callback(false, 0) }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun getQuerySinceDate(n: Int): Date {
        return Utils.nDaysAgo(n)
    }

    private fun getQueryTillDate(n: Int): Date {
        return Utils.nDaysAgo(n)
    }

    fun check() {
        viewModel.checkAuthorizationStatus()
    }


}