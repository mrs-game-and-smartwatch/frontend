package com.mrsgameandsmartwatch.trainingtroop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider


class MainActivity : AppCompatActivity() {


    private var viewModel: HiHealthViewModel? = null
    private var hiHealth: HiHealthConnector? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        viewModel = ViewModelProvider(this)[HiHealthViewModel::class.java]
        fun callback(success: Boolean, score: Int) {
            Log.v("yaay got something", score.toString());
        }
        hiHealth = HiHealthConnector(viewModel!!, this, ::callback)
        hiHealth!!.setup()

    }

    fun signUp(view: View) {
        // On click Sign Up Button
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    fun signIn(view: View) {
        // On click Sign In Button
        val intent = Intent(this, SignInActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        hiHealth!!.check()
    }


}