package com.mrsgameandsmartwatch.trainingtroop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class SignUpActivity : AppCompatActivity() {
    var genderPosition = -1
    var fitLevel = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<Spinner>(R.id.dropdown_gender).onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    genderPosition = position
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    genderPosition = -1
                }
            }

        findViewById<SeekBar>(R.id.scale_fitlevel)
            .setOnSeekBarChangeListener(
                object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(bar: SeekBar, progress: Int, user: Boolean) {
                        fitLevel = progress
                    }

                    override fun onStartTrackingTouch(bar: SeekBar) {}

                    override fun onStopTrackingTouch(bar: SeekBar) {}
                })

    }

    @Suppress("UNUSED_PARAMETER")
    fun showDatePickerDialog(v: View) {
        val eText = findViewById<EditText>(R.id.input_dob)
        val newFragment = DatePickerFragment(eText)
        newFragment.show(supportFragmentManager, "datePicker")
    }

    private fun getISODob() : String {
        val date_split = findViewById<EditText>(R.id.input_dob).text.split("/")
        var day = date_split[0]
        if (date_split[0].length == 1) {
            day = "0" + date_split[0]
        }
        var month = date_split[1]
        if (date_split[1].length == 1) {
            month = "0" + date_split[1]
        }
        return date_split[2] + "-" + month + "-" + day
    }

    @Suppress("UNUSED_PARAMETER")
    fun signUp(v: View) {
        val jsonObj = JSONObject()
        val username = findViewById<EditText>(R.id.input_username).text
        jsonObj.put("username", username)
        val dob = getISODob()
        jsonObj.put("birth_date", dob)
        val gender = when (genderPosition) {
            1 -> 4
            2 -> 2
            3 -> 1
            else -> -1
        }
        jsonObj.put("gender", gender)
        val location = findViewById<EditText>(R.id.input_location).text
        jsonObj.put("location", location)
        val bio = findViewById<EditText>(R.id.input_bio).text
        jsonObj.put("bio", bio)
        Log.e("SIGNUP", "bio: " + jsonObj.get("bio"))
        val score = (fitLevel + 1) * 100 // TODO better calculation
        jsonObj.put("score", score)


        val table = findViewById<TableLayout>(R.id.table_schedule)
        var schedule = 0
        for (i in 1..2) {
            for (j in 0..6) {
                if (((table.getChildAt(i) as TableRow).getChildAt(j) as ToggleButton).isChecked) {
                    // 0th bit of 14 bits is the first button
                    val shift = (13 - (2 * j + (i - 1)))
                    val bitmask = 1 shl shift
                    schedule = schedule or bitmask
                }
            }
        }
        jsonObj.put("available_times", schedule)

        val networking = Networking.getInstance(this)
        val url = "${networking.server}/api/profiles/"

        val jsonRequest =
            JsonObjectRequest(
                Request.Method.POST, url, jsonObj,
                Response.Listener<JSONObject> { response ->
                    Log.d("SIGNUP", "Volley: All good")
                },
                Response.ErrorListener { error ->
                    Log.e("SIGNUP", "Volley Error: $error")
                    Toast.makeText(applicationContext, "Networking error", Toast.LENGTH_SHORT)
                        .show()
                })
        networking.addToRequestQueue(jsonRequest)
    }
}
