package com.mrsgameandsmartwatch.trainingtroop

import android.content.Context
import android.content.res.ColorStateList
import android.text.format.DateFormat
import android.util.Log
import android.widget.ImageView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import java.time.Duration
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.floor

enum class Gender(val value: Int) {
    MALE(4),
    FEMALE(2),
    DIVERSE(1),
}

class User(context: Context, response: JSONObject) {
    inner class Match(context: Context, val id: Int, user: Int) {
        lateinit var username: String
        var score = 0
        var age = 0
        var gender = Gender.MALE
        lateinit var city: String
        lateinit var bio: String
        var schedule = 0
        var verified = false
        var userId = 0

        init {
            val networking = Networking.getInstance(context)
            val server = networking.server
            val url = "$server/api/matches/$id"

            val request = JsonObjectRequest(Request.Method.GET, url, null,
                Response.Listener<JSONObject> {response ->

                    userId = when(val userA = response.getInt("user_a")) {
                        user -> response.getInt("user_b")
                        else -> userA
                    }
                    val innerURL = "$server/api/profiles/$userId"

                    // TODO don't show if already accepted
                    val innerRequest = JsonObjectRequest(Request.Method.GET, innerURL, null,
                        Response.Listener<JSONObject> { innerResponse ->
                            username = innerResponse.getString("username")
                            score = innerResponse.getInt("score")

                            age = innerResponse.getInt("age")
                            gender = when(innerResponse.getInt("gender")) {
                                2 -> Gender.FEMALE
                                1 -> Gender.MALE
                                else -> Gender.DIVERSE
                            }
                            city = innerResponse.getString("city")
                            bio = innerResponse.getString("bio")
                            schedule = innerResponse.getInt("available_times")

                            verified = innerResponse.getBoolean("verified")
                        },

                        Response.ErrorListener { innerError ->
                            Log.e("MATCHGET INNER", innerError.toString())
                        }
                    )
                    networking.addToRequestQueue(innerRequest)
                },

                Response.ErrorListener { error ->
                    Log.e("MATCHGET", error.toString())
                })

            networking.addToRequestQueue(request)
        }

    }

    val id: Int = response.getInt("id")
    var matches: ArrayList<Match> = arrayListOf()
    var matchPos: Int = 0

    init {
        val jmatches = response.getJSONArray("match_obj_ids")
        for (i in 0 until jmatches.length()) {
            matches.add(Match(context, jmatches.getJSONObject(i).getInt("id"), id))
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: User? = null
        fun getInstance() =
                INSTANCE!!

        fun createInstance(context: Context, response: JSONObject) {
            INSTANCE = User(context, response)
        }
    }

    fun getCurrentMatch(): Match {
        return matches[matchPos]
    }

    fun incrMatchPointer() {
        if (matchPos == matches.size - 1) {
            Log.w("MATCHINCR", "No more matches")
            return
        }
        matchPos++;
    }
}