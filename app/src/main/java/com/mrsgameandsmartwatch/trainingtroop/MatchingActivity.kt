package com.mrsgameandsmartwatch.trainingtroop

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject


// TODO this whole thing should probably be fragments if done right
class MatchingActivity : AppCompatActivity() {
    private fun setSchedule(bitmask: Int) {
        var mutBitmask = bitmask
        val table = findViewById<TableLayout>(R.id.schedule_table)
        for (j in 6 downTo 0) {
            for (i in 2 downTo 1) {
                val cell = (table.getChildAt(i) as TableRow).getChildAt(j)
                if (mutBitmask and 1 != 0) {
                    cell.backgroundTintList = ColorStateList.valueOf(
                        resources.getColor(R.color.matchGreen, null)
                    )
                } else {
                    cell.backgroundTintList = ColorStateList.valueOf(
                        resources.getColor(R.color.lightGray, null)
                    )
                }
                mutBitmask = mutBitmask shr 1
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matching)

        // Setup toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        getUserData()
    }

    private fun getUserData() {

        val match = User.getInstance().getCurrentMatch()
        GlobalScope.launch {
            setSchedule(match.schedule)
        }

        findViewById<TextView>(R.id.text_username).text = match.username
        findViewById<TextView>(R.id.text_score).text = match.score.toString()
        findViewById<TextView>(R.id.bio_text).text = match.bio
        findViewById<TextView>(R.id.number_age).text = match.age.toString()
        findViewById<TextView>(R.id.gender).text = when (match.gender) {
            Gender.MALE -> "Male"
            Gender.FEMALE -> "Female"
            Gender.DIVERSE -> "Diverse"
        }
        findViewById<TextView>(R.id.locationtext).text = match.city

        if (match.verified) {
            val image = findViewById<ImageView>(R.id.unverified_warning)
            image.setImageResource(R.drawable.ic_check_circle_black_24dp)
            image.imageTintList = ColorStateList.valueOf(
                resources.getColor(R.color.matchGreen, null)
            )
        }

    }

    // Handle pressing <- on toolbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    // Handle pressing options menu on toolbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_matching, menu)
        return true
    }

    // Handle menu clicks
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_settings -> {
                goToMatchSettings(); true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun goToMatchSettings() {
        val intent = Intent(this, MatchingSettingsActivity::class.java)
        startActivity(intent)
    }

    private fun sendDecision(accept: Boolean) {
        val userId = User.getInstance().id
        val networking = Networking.getInstance(this)
        val url = "${networking.server}/api/profiles/$userId/accept-match/"
        val params = JSONObject()
        params.put("accept", if (accept) "1" else "0")
        params.put("id", User.getInstance().getCurrentMatch().userId)

        Log.e("MATCH", "URL: $url")
        Log.e("MATCH", "Params: $params");

        val jsonRequest = JsonObjectRequest(
            Request.Method.POST, url, params,
            Response.Listener<JSONObject> { response ->
                Log.d("MATCH", "Volley: All good")
                if (response.getBoolean("success")) {
                    Toast.makeText(applicationContext,
                        "Added to group ${response.getString("name")}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            },
            Response.ErrorListener { error ->
                Log.e("MATCH", "Volley Error: $error")
                Toast.makeText(applicationContext, "Networking error", Toast.LENGTH_SHORT)
                    .show()
            })
        networking.addToRequestQueue(jsonRequest)

        User.getInstance().incrMatchPointer()
        getUserData()
    }

    fun acceptMatch(v: View) {
        sendDecision(true)
    }

    fun declineMatch(v: View) {
        sendDecision(false)
    }

}